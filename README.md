Markdown
Markdown es un lenguaje de marcado y una forma sencilla de agregar formato a textos web. Con formato nos referimos a itálicas, negritas, listas, y demás. Markdown fue creado originalmente por John Gruber, con ayuda de Aaron Swartz, con el propósito de crear un texto plano fácil de escribir y fácil de leer, y que pudiera convertirse de forma sencilla y válida a XHTML.

Tabla de contenido
Insertar HN.
Cursivas.
Negrilla.
Viñetas para tablas de contenido.
insertar imágenes.
Insertar enlaces.
Hacer anclaje.
Insertar una línea de código.
Insertar un bloque de código.
Resaltar el código.
Insertar tablas.
Otras referencias sobre Markdown.
Insertar HN
# Esto es un H1
## Esto es un H2
### Esto es un H3
#### Esto es un H4

Cursivas
*Esto es cursiva*

Negrilla
**Esto es negrilla**

Vinetas

- Esto es viñeta 1.
  - Viñeta 1.1 con sangria.
  - Viñeta N.
  
Insertar imagenes
![texto cualquiera por si no carga la imagen](url completa de la imagen)

Insertar enlaces
[texto a mostrar](url completa)

Hacer anclaje
Usar los títulos con la almohadilla # y para anclar el título a una tabla de contenido, ponemos lo siguiente:

[texto a mostrar](#mi-titulo-a-anclar)

Insertar una linea de codigo
Encerrar la linea de código entre la tilde al revés ` Código en ASCII: alt96

Ejemplo:

`tu linea de codigo`
Insertar un bloque de codigo
Encerrar el bloque de código entre tres tildes al revés ``` Código en ASCII: alt96

Ejemplo:

		```
		
		//bloque de codigo...
		
		```
Resaltar el codigo
Encerramos el bloque de código con las tres tildes al revés ``` y le ponemos al lado el lenguaje que se está usando, ejemplo:

		```java
		
		//bloque de codigo...
		
		```
Insertar tablas

| TITULO1| TITULO2|
| ----- | ---- |
| CONTENIDO COLUMNA 1 | CONTENIDO COLUMNA 2 |


Otras referencias sobre Markdown:
http://www.genbeta.com/guia-de-inicio/que-es-markdown-para-que-sirve-y-como-usarlo

https://help.github.com/articles/markdown-basics

https://guides.github.com/features/mastering-markdown/

http://bitelia.com/2013/04/que-es-markdown

http://es.wikipedia.org/wiki/Markdown
